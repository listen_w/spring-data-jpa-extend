package cn.org.wangchangjiu.jpa.extend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaExtendSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaExtendSpringBootStarterApplication.class, args);
    }

}
